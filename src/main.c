#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <wayland-server.h>

#include <wlr/backend.h>


struct motif_server_t {
  struct wl_display *wl_display;
  struct wl_event_loop *wl_event_loop;
  struct wlr_backend *backend;
};


int main(int argc, char **argv) {
  printf("I'll be an awesome mobile window manager someday!\n");

  struct motif_server_t server;

  server.wl_display = wl_display_create();
  assert(server.wl_display);

  server.wl_event_loop = wl_display_get_event_loop(server.wl_display);
  assert(server.wl_event_loop);

  server.backend = wlr_backend_autocreate(server.wl_display, NULL);
  assert(server.backend);

  if (!wlr_backend_start(server.backend)) {
    fprintf(stderr, "Failed to start backend\n");
    wl_display_destroy(server.wl_display);
    return 1;
  }

  wl_display_run(server.wl_display);
  wl_display_destroy(server.wl_display);

  return 0;
}
